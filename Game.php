<?php
class Game
{
	public $gameData;
	public static $diagonal=3;

	public function assignName($username1,$username2)
	{
		$this->gameData[]=$username1;
		$this->gameData[]=$username2;
	}

	protected function chooseMark()
	{	
		if (count($this->gameData)<=2)
		{
			return "X";
		}elseif ($this->gameData[count($this->gameData)-1]=="X")
		{
			return "O";	
		}else
		{
			return "X";
		}
	}

	protected function chekFreeCell ($numberCell)
	{
		for ($numberGameData=2;$numberGameData<count($this->gameData);$numberGameData=$numberGameData+2)
		{
			if ((int)$this->gameData[$numberGameData]==$numberCell)
			{
				return false;
			}
		}
		return true;
	}
		
	
	
	public function makeStep($numberCell)
	{
		if($this->chekFreeCell($numberCell))
		{ 
			$mark=$this->chooseMark();
			$this->gameData[]=$numberCell;
			$this->gameData[]=$mark;
		}
	}	
}
?>