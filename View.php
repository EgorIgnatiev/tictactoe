<?php
class View 
{

	public function showTitle()
	{
		echo "<!DOCTYPE>
		<html>
		<head>
		<title>Крестики-нолики</title>
		</head>
		<body>	";
	}

	protected $buttonValues;
	
	public function showIdentification()
	{	
		?>
		<form action="?action=game" method="post">
		<input type="text" name="username1">
		<input type="text" name="username2">
		<input type="submit" value="Сохранить">
		</form>
		<?php	
	}	

	protected function showUsername($Game)	
	{
		?>
		<form>
		ИгрокX:<input type="text" name="Игрок1" value="<?php echo $Game->gameData[0];?>">
		ИгрокO:<input type="text" name="игрок2" value="<?php echo $Game->gameData[1];?>">
		</form>
		<?php		
	}
	
	
	protected function determinationButtonValues($Game)
	{
		$Data=$Game->gameData;
		$this->buttonValues=array_chunk($Data,2);
	}

	protected function showValueButton($Game,$numberColumn,$numberLine)
	{
		$this->determinationButtonValues($Game);
		$numberButton=$numberColumn+($numberLine-1)*$Game::$diagonal;
		for($numberValuesButton=1;$numberValuesButton<count($this->buttonValues);$numberValuesButton++)
		{
			if 	(($numberButton)==($this->buttonValues[$numberValuesButton][0]))
			{
				return $this->buttonValues[$numberValuesButton][1];
			}
		}
		return " ";
	}
		

	protected function showGameboard($Game)
	{
		?>
		<form action="?action=game"  method="post">
		<table>
		<?php
		for ($numberLine=1;$numberLine<=$Game::$diagonal;$numberLine++)
		{
			?>
			<tr>
			<?php
			for($numberColumn=1;$numberColumn<=$Game::$diagonal;$numberColumn++)
			{
				?>
				<th><input type="submit" name="<?php echo ($numberColumn+($numberLine-1)*$Game::$diagonal);?>" value="<?php $value=$this->showValueButton($Game,$numberColumn,$numberLine); echo $value; ?>"></th>
				<?php
			}
			?>
			</tr>
			<?php
		}
		?>
		</table>
		</form>
		<?php
	}

	public function showGame($Game)
	{
		$this->showUsername($Game);
		$this->showGameboard($Game);
	}
	
	public function showEndGame($winer)
	{
		?>
		<form action="?action=end"  method="post">
			Победитель:<input type="text" name="Победитель" value="<?php echo $winer;?>">
		</form>
		<?php
	}
	
	
}
?>