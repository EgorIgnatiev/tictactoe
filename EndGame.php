<?php
class EndGame
{
	public $winer;	
	protected $orderCell;
	private $demensionBoard;

	public function finishGame($Game)
	{
		$this->createOrderCell($Game);
		$this->$demensionBoard=$Game::$diagonal;
		
		if($this->wins())
		{
			$this->winer=$this->whoWins();
			return true;
		}
		if($this->noWiners())
		{
			$this->winer="Отсутсвует";
			return true;
		}
		return false;
	}

	protected function createOrderCell($Game)
	{
		$Data=$Game->gameData;
		$steps=array_chunk($Data,2);
		$username=array_shift($steps);
		sort($steps);
		for($numberStep=0;$numberStep<count($steps);$numberStep++)
		{
			$this->orderCell[]=$steps[$numberStep][1];
		}
	}
	
	private function wins()
	{
		if ($this->endStrok())
		{
		return true;	
		}
		if ($this->endStolb())
		{
		return true;
		}
		if ($this->endDiagonale())
		{
		return true;	
		}
		return false;
	}

	private function noWiners()
	{
		if (count($this->orderCell)==9)
		{
			return true;
		}
		return false;
	}

	private function whoWins()
	{
		$arrayValue=$this->sortStep();
		if ($this->endStrok($arrayValue))
		{
		return $this->endStrok($arrayValue);
		}
		if ($this->endStolb($arrayValue))
		{
		return $this->endStolb($arrayValue);	
		}
		if ($this->endDiagonale($arrayValue))
		{
		return $this->endDiagonale($arrayValue);	
		}
	}

	private function endStrok()
	{
		for ($numberLine=0; $numberLine<=count($this->orderCell); $numberLine+=$this->$demensionBoard)
		{
			$checkVariebel=0;
			for ($numberColumn=1;$numberColumn<=$this->$demensionBoard-1;$numberColumn++)
			{
				if ($this->orderCell[$j+$i]==$arrayValue[$j+1+$i])
				{
					$checkVariebel++;
				}
			}
			if ($checkVariebel==$this->$demensionBoard-1)
			{
				$this->winer=$this->orderCell[$j+$i];
				return true;
			}
		}
	}

	private function endStolb($arrayValue)
	{
	for ($i=0; $i<Gameboard::$diagonal; $i++)
	{
		$checkVariebel=0;
		for ($j=0;$j<Gameboard::$diagonal-1;$j++)
		{
			if ($arrayValue[$j*Gameboard::$diagonal+$i]==$arrayValue[($j+1)*Gameboard::$diagonal+$i])
			{
			$checkVariebel++;
			}
		}
		if ($checkVariebel==Gameboard::$diagonal-1)
		{
			return $arrayValue[$i];
		}
	}
	}

	private function endDiagonale($arrayValue)
	{
		$checkVariebel=0;
	for ($i=0; $i<count($arrayValue)-1; $i+=Gameboard::$diagonal+1)
	{	
		if ($arrayValue[$i]==$arrayValue[$i+Gameboard::$diagonal+1])
			{
			$checkVariebel++;
			}

	}
	if ($checkVariebel==(Gameboard::$diagonal-1))
	{
		return $arrayValue[0];
	}
		$checkVariebel=0;
	for ($i=Gameboard::$diagonal-1; $i<count($arrayValue); $i+=Gameboard::$diagonal-1)
	{	
		if ($arrayValue[$i]==$arrayValue[$i+Gameboard::$diagonal-1])
			{
			$checkVariebel++;
			}

	}
	if ($checkVariebel==(Gameboard::$diagonal-1))
	{
		return $arrayValue[Gameboard::$diagonal-1];
	}
	return "";
	}
	private function sortStep()
	{
		$filledCell=Repository::loadAllStep();
		sort($filledCell);
		for($i=0; $i<count($filledCell); $i++){
		$filledCell[$i]=explode(" ",$filledCell[$i])[1];
		}
	return $filledCell;
	}
}